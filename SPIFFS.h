#define SPIFFS_DEBUG true
#define SPIFFS_NA "N.A."
#include <FS.h>

#define LOGFILE "log"

String spiffs_getValue(String file) {
    File f = SPIFFS.open(file, "r");
    if (!f) {
        Serial.println("file open failed");
        return SPIFFS_NA;
    }
    return f.readStringUntil('\n');
}

int spiffs_writeValue(String file,String value) {
    File f = SPIFFS.open(file, "w");
    if (!f) {
        Serial.println("file open failed");
        return -2;
    }
    return f.print(value);
}

int spiffs_appendValueNL(String file,String value) {
    File f = SPIFFS.open(file, "a");
    if (!f) {
        Serial.println("file open failed");
        return -2;
    }
    return f.println(value);
}

int spiffs_log(String v) {
    spiffs_appendValueNL(LOGFILE,v);
}

int spiffs_writeValue(String file,int value) {
    spiffs_writeValue(file, String(value));
}

/*
void spiffs_getValues() {
    String tmp=spiffs_getValue("/presenze");
    if(tmp!=SPIFFS_NA) presenze=tmp.toInt(); // attenzione che se non e' un numero torna 0!!!
}
*/

int spiffs_getPresenze() {
    String tmp=spiffs_getValue("/presenze");
    if(tmp!=SPIFFS_NA) return tmp.toInt(); // attenzione che se non e' un numero torna 0!!!
}
