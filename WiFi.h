// 21626c macaddress per distinguere OTA

// WiFi & NTP & OTA
#include <ESP8266WiFi.h>
#include <ESP8266WebServer.h>
//#include <ESP8266mDNS.h>
#include <ArduinoOTA.h>
#include <NTPClient.h>
#include <WiFiUdp.h>
#include <WiFiClient.h>
//#include <SPI.h>
//#include <SD.h>

WiFiClient wifiClient;
IPAddress gateway;
byte mac[6];

WiFiUDP ntpUDP;
// ntp ci serve per i timestamp!!!
NTPClient timeClient(ntpUDP,+60*60); // UTC+1

String ssid = "hacklabcormano.it";
String password = "arduino2linux";
//#include "account.h"

#define WIFI_TENTATIVI 100
#define USE_GW "[gw]"

ESP8266WebServer webserver(80);

FtpServer ftpSrv;   //set #define FTP_DEBUG in ESP8266FtpServer.h to see ftp verbose on serial

void returnOK() {
    webserver.send(200, "text/plain", "");
}

void returnFail(String msg) {
    webserver.send(500, "text/plain", msg + "\r\n");
}

void test() {
    if(!webserver.hasArg("test")) return returnFail("BAD ARGS");

    //String path = webserver.arg("dir");

    webserver.setContentLength(CONTENT_LENGTH_UNKNOWN);
    webserver.send(200, "text/json", "");

    WiFiClient client = webserver.client();

    webserver.sendContent("timestamp:");
    webserver.sendContent(timeClient.getFormattedTime());
}

void handleNotFound() {
    /*
    String message = "SDCARD Not Detected\n\n";
    message += "URI: ";
    message += webserver.uri();
    message += "\nMethod: ";
    message += (webserver.method() == HTTP_GET)?"GET":"POST";
    message += "\nArguments: ";
    message += webserver.args();
    message += "\n";
    for (uint8_t i=0; i<webserver.args(); i++){
    message += " NAME:"+webserver.argName(i) + "\n VALUE:" + webserver.arg(i) + "\n";
    }
    */
    webserver.send(404, "text/plain", "not found");
}


boolean wifi_setup() {
    delay(500);

    WiFi.mode(WIFI_STA);

    // We start by connecting to a WiFi network
    Serial.print("=== Connecting to: ");
    Serial.println(ssid);

    WiFi.disconnect();
    WiFi.begin(ssid.c_str(), password.c_str());

    for (int i=0; (WiFi.status() != WL_CONNECTED) && (i < WIFI_TENTATIVI) && digitalRead(0)==HIGH; i++) {
        delay(500);
        Serial.print(".");
    }

    if(WiFi.status() == WL_CONNECTED) {
        Serial.println();
        Serial.println("+++ WiFi connected!");

        Serial.print("IP: ");
        Serial.println(WiFi.localIP());

        Serial.print("GW: ");
        Serial.println(WiFi.gatewayIP());

        timeClient.begin();
        delay(500);
        timeClient.update();

        // Port defaults to 8266
        ArduinoOTA.setPort(8266);

        // Hostname defaults to esp8266-[ChipID]
        // ArduinoOTA.setHostname("myesp8266");

        // No authentication by default
        // ArduinoOTA.setPassword((const char *)"123");

        ArduinoOTA.onStart([]() {
            Serial.println("OTA Start");
        });
        ArduinoOTA.onEnd([]() {
            Serial.println("OTA End");
        });
        ArduinoOTA.onProgress([](unsigned int progress, unsigned int total) {
            Serial.printf("OTA Progress: %u%%\r", (progress / (total / 100)));
            //bar(strip.Color(0,0,200),ledIndex(NUM_LEDS*progress/total));
        });
        ArduinoOTA.onError([](ota_error_t error) {
            Serial.printf("OTA Error[%u]: ", error);
            if (error == OTA_AUTH_ERROR) Serial.println("Auth Failed");
            else if (error == OTA_BEGIN_ERROR) Serial.println("Begin Failed");
            else if (error == OTA_CONNECT_ERROR) Serial.println("Connect Failed");
            else if (error == OTA_RECEIVE_ERROR) Serial.println("Receive Failed");
            else if (error == OTA_END_ERROR) Serial.println("End Failed");
        });

        ArduinoOTA.begin();
        Serial.println("+++ OTA Ready");

        webserver.on("/test", HTTP_GET, test);
        //webserver.on("/edit", HTTP_DELETE, handleDelete);
        //webserver.on("/edit", HTTP_PUT, handleCreate);
        //webserver.on("/edit", HTTP_POST, [](){ returnOK(); }, handleFileUpload);
        webserver.onNotFound(handleNotFound);

        webserver.begin();
        Serial.println("HTTP server started");

        /////FTP Setup, ensure SPIFFS is started before ftp;  /////////
#ifdef ESP32       //esp32 we send true to format spiffs if cannot mount
        if (SPIFFS.begin(true)) {
#elif defined ESP8266
        if (SPIFFS.begin()) {
#endif
            Serial.println("SPIFFS opened!");

            ftpSrv.begin("contapersone","contapersone");    //username, password for ftp.  set ports in ESP8266FtpServer.h  (default 21, 50009 for PASV)
            Serial.println("FTP started");
        }

        return true;
    } else {
        Serial.println("--- WiFi FAILED!");
        return false;
    }
}



void webAndFtpServer() {
    webserver.handleClient(); // Web server
    ftpSrv.handleFTP();    // FTP server
}
