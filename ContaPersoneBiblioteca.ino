/**
    ContaPersone per biblioteca Cormano

	Copyright (C) 2018 HackLabCormano (http://hacklabcormano.it)

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.*
*/

// TODO filesystem save dati
// cfr. https://github.com/esp8266/Arduino/blob/master/doc/filesystem.rst
// immaginando di usare 13 car a evento e circa 1000 eventi al giorno ci stanno circa 220 gg in 3M

// https://github.com/nailbuster/esp8266FTPServer !!!

// TODO uniformare a inglese

// TODO web interface che mostra il log, meglio ancora: web interface che supporta dir, rename, etc.




/*
 * due "stream" dati:
 * - mqtt (ma non vincolante), pubblica varie notizie, in ordine sparso e con tempistiche varie
 * 		timestamp (solo quando sync ntp, ogni ora)
 * 		heap (debugging, "sono vivo", ogni minuto circa)
 * 		i/u (all'evento)
 * 		presenze (all'evento o in altro caso, RETAINED)
 * - logfile (così è anche selfcontained e non abbiamo dipendenze da servizi esterni)
 * 		timestamp (ogni tanto)
 * 		i/u
 * 		i/u
 * 		i/u
 * 		...
 */

#include <TaskScheduler.h>
#include <ESP8266FtpServer.h>

#include "WiFi.h"
#include "MQTT.h"
#include "SPIFFS.h"
#include "Sensori.h"

Scheduler runner;

/** per avere un feedback di funzionamento anche via mqtt */
void statusMqtt() {
    Serial.print("heap: ");
    Serial.println(ESP.getFreeHeap());
    mqtt_send(TOPIC "heap",String(ESP.getFreeHeap()));
}
Task taskMqtt(TASK_MINUTE, TASK_FOREVER, statusMqtt);

/** sync ntp, controlla anche wifi, manda mqtt status e logga timestamp su file */
void syncNTPetc() {
    if (WiFi.status() != WL_CONNECTED) {
        Serial.println("no wifi!");
        ESP.reset();
    }

    timeClient.update();    // sync NTP

    Serial.print("Time: ");
    Serial.println(timeClient.getFormattedTime());
    Serial.print("EpochTime: ");
    Serial.println(timeClient.getEpochTime());

    mqtt_send(TOPIC "time",timeClient.getFormattedTime());
    spiffs_log(timeClient.getFormattedTime());

}
Task taskSyncNTPetc(TASK_HOUR, TASK_FOREVER, syncNTPetc); // ogni ora aggiorna ntp

Task taskPubblicaPresenze(TASK_SECOND, TASK_FOREVER, pubblicaPresenze);

Task taskWeb(TASK_SECOND, TASK_FOREVER, webAndFtpServer);

///////////////////////////////////////////////////
void setup() {
    Serial.begin(115200);
    Serial.println("booting...");

    // vari setup
    wifi_setup();
    mqtt_setup();
    sensori_setup();

    // Tasks
    runner.init();

    runner.addTask(taskPubblicaPresenze);
    taskPubblicaPresenze.enable();

    runner.addTask(taskWeb);
    taskWeb.enable();

    runner.addTask(taskMqtt);
    taskMqtt.enable();

    runner.addTask(taskSyncNTPetc);
    taskSyncNTPetc.enable();

    Serial.println("booted!");
}

void loop() {
    runner.execute();
    ArduinoOTA.handle(); // IMPORTANTE, non togliere!!!
}
