// MQTT
// la lib è PubSubClient (https://pubsubclient.knolleary.net/)
#include <PubSubClient.h>  // mqtt

#define TOPIC "ContaPersone"
String mqtt_server = "mqtt.hacklabcormano.it";

PubSubClient mqtt_client(wifiClient);
#define DELAY_MQTT 1000
long lastMsg = 0;
#define MSG_LEN 150
char msg[MSG_LEN];

void mqtt_send(String topic,String payload);

void mqtt_reconnect() {
    // Loop until we're reconnected
    if (!mqtt_client.connected()) {
        Serial.print("Attempting MQTT connection...");
        if (mqtt_client.connect("ContaPersone")) {  // si connette al BROKER dichiarando "identità"
            Serial.println("connected");

            // Once connected, publish an announcement...
            mqtt_send(TOPIC,"connected");

            // ... and resubscribe
            //Serial.print("subscribing: ");
            //Serial.println(mqtt_client.subscribe("CrowdZitter/#")); // TODO parametrizzare/config

        } else {
            Serial.print("failed, rc=");
            Serial.print(mqtt_client.state());
            //Serial.println(" skipping MQTT...");
            // Wait 5 seconds before retrying
        }
    }
}

void mqtt_send(String topic,String payload) {
    if (WiFi.status() == WL_CONNECTED) {
        if (!mqtt_client.connected()) {
            mqtt_reconnect();
            mqtt_send(TOPIC,"RE-connected");
            Serial.println("reconnected...");
        }

        if (mqtt_client.connected()) {
            //mqtt_client.loop();

            if (mqtt_client.publish(topic.c_str(), payload.c_str())) {
                Serial.println("MQTT published");
            } else {
                Serial.println("MQTT error (connection error or message too large)");
            }
        }
    }
}

void mqtt_setup() {
    mqtt_client.setServer(mqtt_server.c_str(), 1883); // 1883 è porta default MQTT
    //mqtt_client.setCallback(rxMQTT);
    mqtt_reconnect();
    Serial.println("mqtt set");
}
